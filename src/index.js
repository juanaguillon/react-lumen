import React from "react";
import ReactDOM  from 'react-dom';
import "bulma/bulma.sass";
import './style.scss';


import App from "./pages/App";

require("dotenv").config();


const container = document.getElementById("root");

ReactDOM.render(<App/>, container);
