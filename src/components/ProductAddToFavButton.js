import React from "react";
import Auth from "../services/Auth";
import HttpClass from "../services/Http";

/**
 * @param productId ID de producto a ser anadido.
 */
const ProductAddToFavButton = props => {
  let auth = new Auth();
  if (!auth.isLogged()) {
    return "";
  }

  const buttonOnClick = (id,e) => {

    e.preventDefault();
    
    let http = new HttpClass();
    let dataToSend = {
      userID: auth.getDecodeToken()["sub"]["id"],
      productID: id
    };

    http.setBodyAsJSONStringify();
    http
      .post("favorite/create", dataToSend, {
        Authorization: auth.getToken(),
        "Content-Type": "application/json"
      })
      .then(data => {
        if (!data.error && data.code === "product_added") {
          let message = document.querySelector(".active_favorites_message");
          message.classList.add("active");
          setTimeout(() => {
            message.classList.remove("active");
          }, 3000);
        }
      });
  };

  return (
    <button
      className="button_add_fav"
      onClick={buttonOnClick.bind(this, props.productId)}
    >
      <i className="icon-heart" />
    </button>
  );
};

export default ProductAddToFavButton;