import React from "react";

import FormGroup from "./GeneralForms/FormGroup";
import Spinner from "../GeneralComponents/Spinner";
import HttpClass from "../../services/Http";

import "./CreateProduct.Form.css";
import Message from "../GeneralComponents/Message";

class CreateProductForm extends React.Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
    this.state = {
      formData: {},

      // Nombre de imagen del producto
      fileName: "Sin archivo seleccionado",
      loading: false,
      messageShow: false,
      messageText: "",
      messageType: "is-dark"
    };
  }

  handleInputChange = e => {
    let prevState = this.state;
    let formData = { ...this.state.formData };
    formData[e.target.name] = e.target.value;
    prevState.formData = formData;

    this.setState(prevState);
  };

  handleInputFileChange = e => {
    let prevState = this.state;
    let formData = { ...this.state.formData };

    formData[e.target.name] = e.target.files[0];
    prevState["formData"] = formData;

    if (e.target.files.length > 0) {
      prevState["fileName"] = e.target.files[0].name;
    } else {
      prevState["fileName"] = "Sin archivo seleccionado";
    }

    this.setState(prevState);
  };

  createImagesAsBase64 = (file, callback) => {
    let image = new FileReader();
    image.onload = function(eImg) {
      let src = eImg.target.result;
      let imgElement = document.createElement("img");
      imgElement.src = src;
      callback(imgElement);
    };
    image.readAsDataURL(file);
  };

  /**
   * Cuando el input de la galería cambie.
   */
  handleChangeGalleryInput = e => {
    let length = e.target.files.length;
    if (length > 0 && length <= 5) {
      const galleryContainer = document.querySelectorAll(
        ".gallery_images_container"
      );

      for (let i = 0; i < e.target.files.length; i++) {
        if (i === 5) break;
        this.createImagesAsBase64(e.target.files[i], imageHTML => {
          galleryContainer[i].innerHTML = "";
          galleryContainer[i].appendChild(imageHTML);
        });
      }

      this.setState({
        formData: {
          ...this.state.formData,
          product_gallery: e.target.files
        }
      });
    } else {
      this.setState({
        messageText: "Puedes ingresar solo 5 imágenes",
        messageShow: true
      });
    }
  };

  /**
   * Crear el Scroll hasta el mensaje. Este metodo se llamara en caso de enviar alguna alerta a el enviar el formulario.
   */
  __scrollToMessage = () => {
    window.scrollTo(0, this.inputRef.current.offsetTop);
  }

  handleSubmit = e => {
    e.preventDefault();

    if (!this.state.formData["product_name"]) {
      this.setState({
        messageShow: true,
        messageText: "El nombre del producto no puede ser vacío.",
        messageType: "is-danger"
      });
      this.__scrollToMessage()
      return false;
    } else if (!this.state.formData["product_price"]) {
      this.setState({
        messageShow: true,
        messageText: "El precio del producto no puede ser vacío.",
        messageType: "is-danger"
      });
      this.__scrollToMessage();
      return false;
    }
    this.setState({
      ...this.state,
      loading: true
    });

    var fd = new FormData();
    for (let i in this.state.formData) {
      if (i === "product_gallery") {
        let iterable = this.state.formData[i];
        let index = 0;
        for (let i2 of iterable) {
          fd.append("product_gallery" + index, i2);
          index++;
        }
      } else {
        fd.append(i, this.state.formData[i]);
      }
    }

    let http = new HttpClass();
    http
      .post("product/create", fd, {
        Authorization: localStorage.getItem("token_id")
      })
      .then(resp => {
        console.log(resp);
        if (!resp.error) {
          this.setState({
            messageShow:true,
            messageText: "Se ha creado el producto correctamente",
            messageType: "is-success"
          });
        }else{
          this.setState({
            messageShow: true,
            messageText: resp.message,
            messageType: "is-danger"
          });
        }

        this.__scrollToMessage();
        this.setState({
          loading: false
        });
      });
  };

  render() {
    return (
      <div className="box">
        <div ref={this.inputRef } id="message_refering">
          <Message
            id="message_crete_product"
            show={this.state.messageShow}
            type={this.state.messageType}
          >
            {this.state.messageText}
          </Message>
        </div>
        {this.state.loading && <Spinner />}

        <form id="create_product_form" onSubmit={this.handleSubmit}>
          <FormGroup
            type="text"
            onInputChange={this.handleInputChange}
            text="Nombre de Producto"
            name="product_name"
          />

          <FormGroup
            name="product_description"
            type="textarea"
            onInputChange={this.handleInputChange}
            text="Descripción de Producto"
          />

          <FormGroup
            type="file"
            name="product_image"
            text={this.state.fileName}
            onInputChange={this.handleInputFileChange}
          />
          <div className="field">
            <label htmlFor="product_price" className="label">
              Precio
            </label>
            <div className="field has-addons">
              <div className="control">
                <button className="button is-static">$</button>
              </div>
              <div className="control">
                <input
                  className="input"
                  onChange={this.handleInputChange}
                  type="number"
                  name="product_price"
                  id="product_price"
                />
              </div>
            </div>
          </div>

          <div className="images_container">
            <label className="label">Añade una Galería</label>
            <ul>
              <input
                type="file"
                name="gallery_images[]"
                id="gallery_images"
                multiple
                onChange={this.handleChangeGalleryInput}
              />
              <li>
                <div className="gallery_images_container">
                  <i className="icon-camera" />
                </div>
              </li>
              <li>
                <div className="gallery_images_container">
                  <i className="icon-camera" />
                </div>
              </li>
              <li>
                <div className="gallery_images_container">
                  <i className="icon-camera" />
                </div>
              </li>
              <li>
                <div className="gallery_images_container">
                  <i className="icon-camera" />
                </div>
              </li>
              <li>
                <div className="gallery_images_container">
                  <i className="icon-camera" />
                </div>
              </li>
            </ul>
          </div>

          <FormGroup
            type="submit"
            buttonClass="success"
            name="send_form"
            text="Guardar"
          />

        </form>
      </div>
    );
  }
}

export default CreateProductForm;
