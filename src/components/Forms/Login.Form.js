import React from 'react';

import HttpClass from '../../services/Http';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };

    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeInput(e) {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    let http = new HttpClass();
    http.setBodyAsJSONStringify();
    let json = http.post("user/auth", this.state);
    json.then(f => {
      if (typeof f.token != "undefined") {
        localStorage.setItem("token_id", f.token);
        window.location.href = "/"
      } else {
        console.log(f);
      }
    });
  }

  render() {
    
    return (
      <form id="login_form" onSubmit={this.handleSubmit}>
        <div className="field">
          <label className="label" htmlFor="user_field">Usuario o Email</label>
          <input
            onChange={this.handleChangeInput}
            name="email"
            id="user_field"
            type="text"
            className="input itext"
          />
        </div>
        <div className="field">
          <label className="label" htmlFor="pass_field">Contraseña</label>
          <input
            onChange={this.handleChangeInput}
            name="password"
            id="pass_field"
            type="password"
            className="input itext"
          />
        </div>
        <div className="field">
          <input
            type="submit"
            value="Ingresar"
            className="button is-success"
          />
        </div>
      </form>
    );
  }
} 

export default LoginForm