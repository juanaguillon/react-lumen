import React from "react";
import HttpClass from "../../services/Http";

import Spinner from "../GeneralComponents/Spinner"
import Message from "../GeneralComponents/Message"

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        email: "",
        name: "",
        pass: "",
        rpass: ""
      },
      loading: false,
      messageType: null,
      messageText: null,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(e) {
    let target = e.target;
    let value = target.value;
    let name = target.name;

    let formData = {...this.state.formData}
    formData[name] = value;
    this.setState({
      formData: formData
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({
      loading:true
    });

    let http = new HttpClass();
    http.setBodyAsJSONStringify();
    http.post("user/create", this.state).then(f => {
      if ( f ){
        this.setState({
          loading:false,
          messageType: "is-success",
          messageText: "Se ha registrado el usuario correctamente."
        })
      }

    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state.loading && <Spinner />}
        <Message show={this.state.messageText !== null } type={this.state.messageType}>{this.state.messageText}</Message>
        <form onSubmit={this.handleSubmit} id="register_form">
          <div className="field">
            <label className="label" htmlFor="register_email">
              Email
            </label>
            <input
              type="text"
              id="register_email"
              className="input itext"
              name="email"
              onChange={this.handleInputChange}
            />
          </div>
          <div className="field">
            <label className="label" htmlFor="register_name">
              Nombre completo
            </label>
            <input
              type="text"
              id="register_name"
              className="input itext"
              name="name"
              onChange={this.handleInputChange}
            />
          </div>
          <div className="field">
            <label className="label" htmlFor="register_pass">
              Contraseña
            </label>
            <input
              type="password"
              id="register_pass"
              className="input itext"
              name="pass"
              onChange={this.handleInputChange}
            />
          </div>
          <div className="field">
            <label className="label" htmlFor="register_rpass">
              Repetir Contraseña
            </label>
            <input
              type="password"
              id="register_rpass"
              className="input itext"
              name="rpass"
              onChange={this.handleInputChange}
            />
          </div>
          <div className="field">
            <input
              type="submit"
              value="Registrar"
              className="button is-success"
            />
          </div>
        </form>
      </React.Fragment>
    );
  }
}

export default Register;
