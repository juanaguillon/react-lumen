import React from "react";

/**
 * @param {String} label Texto para seleccionar archivo
 * @param {String} name [id, name] de el input.
 * @param {String} fileName Nombre de archivo
 * @param {Callback} onChange Cuando el archivo cambia
 */
const InputFile = prop => {
  return (
    <div className="file has-name">
      <label htmlFor={prop.name} className="file-label">
        <input
          id={prop.name}
          name={prop.name}
          onChange={prop.onChange}
          className="file-input"
          type="file"
        />
        <span className="file-cta">
          <span className="file-icon">
            <i className="icon-upload" />
          </span>
          <span className="file-label">{prop.label}</span>
        </span>
        <span className="file-name">{prop.fileName}</span>
      </label>
    </div>
  );
};

/**
 * ====== Requeriod
 * @param {any} name Se usará para ajustar el ID y el name del input.
 * @param {string} type Tipo de input
 * @param {string} text Texto que aparecerá en el label.
 *
 * ====== Si es input Text o teaxtarea
 * @param {Callback} onInputChange Cuando el input cambie
 *
 * ====== Si es submit o button
 * @param {string} buttonClass El tipo de botón que será mostrado
 */
const FormGroup = props => {
  let classInput = "";
  let isButton = false;

  // Verificar si el input es tipo botón o submit.
  switch (props.type) {
    case "button":
    case "submit":
      isButton = true;
      classInput = "button is-" + props.buttonClass;
      break;

    case "text":
      classInput = "input";
      break;

    case "textarea":
      classInput = "textarea";
      break;

    case "file":
      classInput = "input ifile";
      break;

    default:
      break;
  }

  /** Si es un textarea */
  if (props.type === "textarea") {
    return (
      <div className="field">
        <label className="label" htmlFor={props.name}>
          {props.text}
        </label>
        <div className="control">
          <textarea
            name={props.name}
            id={props.name}
            type={props.type}
            onChange={props.onInputChange}
            className={classInput}
          />
        </div>
      </div>
    );
  } else if (props.type === "file") {
    return (
      <div className="field">
        <div className="control">
          <InputFile
            label="Seleccionar Archivo"
            fileName={props.text}
            name={props.name}
            onChange={props.onInputChange}
          />
        </div>
      </div>
    );
  }

  return (
    <div className="field">
      {!isButton && (
        <label className="label" htmlFor={props.name}>
          {props.text}
        </label>
      )}
      <div className="control">
        <input
          name={props.name}
          id={props.name}
          type={props.type}
          value={isButton ? props.text : undefined}
          onChange={props.onInputChange}
          className={classInput}
        />
      </div>
    </div>
  );
};

export default FormGroup;
