import React from "react";

/**
 * 
 * @param {bool} show Se estara mostrando el modal
 * @param {string} title Titlo de modal
 * @param {srting} continue_class Clase css para el boton de continuar
 * @param {srting} continue_text Texto para el boton de continuar
 * @param {srting} cancel_class Clase css para el boton de cancelar
 * @param {srting} cancel_text Texto para el boton de continuar
 * @param {Function} onContinue Que hara cuando cliquee en boton Continuar
 * @param {Function} onCancel Que hara cuando cliquee en boton Cancelar
 */
const Modal = x => {

  if ( ! x.show ){
    return "";
  }
  
  return (
    <div className="modal is-active">
      <div className="modal-background" />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{x.title}</p>
          <button onClick={x.onCancel} className="delete" aria-label="close" />
        </header>
        <section className="modal-card-body">{x.children}</section>
        <footer className="modal-card-foot">
          <button
            onClick={x.onContinue}
            className={"button " + x.continue_class}
          >
            {x.continue_text}
          </button>
          <button
            onClick={x.onCancel}
            className={"button " + x.cancel_class}
          >
            {x.cancel_text}
          </button>
        </footer>
      </div>
    </div>
  );
};

export default Modal;
