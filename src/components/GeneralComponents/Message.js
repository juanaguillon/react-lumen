import React from "react";

function Message({ children, type, show, ...props }) {
  if (show) {
    return (
      <div {...props} className={"message " + type}>
        <div className="message-body">{children}</div>
      </div>
    );
  } else {
    return "";
  }
}

export default Message;