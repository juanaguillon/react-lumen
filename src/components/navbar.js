import React from "react";
import { Link } from "react-router-dom";

import Auth from "../services/Auth";

import "./NavBar.scss"

const DynamicLink = ({ show, ...props }) => {
  if (show) {
    return <Link {...props}>{props.children}</Link>;
  } else {
    return "";
  }
};

class NavBar extends React.Component {
  auth = new Auth();

  handleCloseSesion = e => {
    this.auth.closeSession();
    this.forceUpdate();
  };

  render() {
    return (
      <nav
        className="navbar is-dark"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <a className="navbar-item" href="https://bulma.io">
            <img
              src="https://bulma.io/images/bulma-logo.png"
              width="112"
              height="28"
              alt=""
            />
          </a>

          <button
            className="navbar-burger burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </button>
        </div>

        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <Link to="/" className="navbar-item">
              Inicio
            </Link>

            <DynamicLink
              show={this.auth.isLogged()}
              to="/private"
              className="navbar-item"
            >
              Privado
            </DynamicLink>

            <DynamicLink
              show={this.auth.isLogged()}
              to="/product/create"
              className="navbar-item"
            >
              Crear Producto
            </DynamicLink>
            <Link to="/product/all" className="navbar-item">
              Productos
            </Link>

            {/* <div className="navbar-item has-dropdown is-hoverable">
              <a className="navbar-link">More</a>

              <div className="navbar-dropdown">
                <a className="navbar-item">About</a>
                <a className="navbar-item">Jobs</a>
                <a className="navbar-item">Contact</a>
                <hr className="navbar-divider" />
                <a className="navbar-item">Report an issue</a>
              </div>
            </div> */}
          </div>

          <div className="navbar-end">
            <div className="navbar-item">
              <div className="active_favorites_wrap">
                <div className="active_favorites_container">
                  <div className="active_favorites_icon">
                    <DynamicLink
                      className="button button_favorites is-dark "
                      to="/favorite/all"
                      show={this.auth.isLogged()}
                    >
                      <i className="icon-heart" />
                      <span>Tus Favoritos</span>
                    </DynamicLink>
                    <div className="active_favorites_message">
                      <span className="has-text-white is-size-7">
                        Se ha agregado a tu lista de favoritos
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="buttons">
                <DynamicLink
                  className="button is-primary"
                  to="/register"
                  show={!this.auth.isLogged()}
                >
                  <strong>Registrar</strong>
                </DynamicLink>
                <DynamicLink
                  className="button is-light"
                  to="/login"
                  show={!this.auth.isLogged()}
                >
                  Ingresar
                </DynamicLink>
                <DynamicLink
                  className="button is-info"
                  to="/login"
                  onClick={this.handleCloseSesion}
                  show={this.auth.isLogged()}
                >
                  Cerrar Sesión
                </DynamicLink>
              </div>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;
