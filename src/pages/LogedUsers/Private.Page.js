import React from "react";
import { Redirect } from "react-router-dom";

import Auth from "../../services/Auth";

class Private extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      noLogin: false
    };
  }

  componentDidMount = () => {
    let auth = new Auth();
    if (!auth.isLogged()) {
      this.setState({
        noLogin: false
      });
    }
  };

  render() {
    if (this.state.noLogin) return <Redirect to="/" />;

    return "Hola, es el privado";
  }
}

export default Private;
