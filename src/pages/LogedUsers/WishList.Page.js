import React from "react";
import "./WishList.Page.scss";

// Services
import HttpClass from "../../services/Http";
import Auth from "../../services/Auth";
import generalFunctions from "../../services/Functions";

// Componenets
import Modal from "../../components/GeneralComponents/Modal";
import Message from "../../components/GeneralComponents/Message";
import Spinner from "../../components/GeneralComponents/Spinner";

const WishListSingle = props => {
  return (
    <div className="wish_list_single">
      <div className="wish_list_wrap">
        <div className="wish_list_image_wrap">
          <img src={props.src} alt={props.title} className="image is-128x128" />
        </div>
        <div className="wish_list_context">
          <div className="wish_list_title_wrap">
            <h3 className="title is-4">{props.title}</h3>
          </div>
          <div className="wish_list_price">
            <span>{generalFunctions.formatCurrency(props.price)}</span>
          </div>
        </div>
        <div className="wish_list_date">
          <span>Añadido:</span>
          <time>{props.created_at}</time>
        </div>
        <div className="wish_list_remove_item">
          <button
            onClick={props.onRemove}
            className="wish_list_button_remove_item button is-small"
          >
            <i className="icon-exit has-text-danger" />
          </button>
        </div>
      </div>
    </div>
  );
};

class WishListPage extends React.Component {
  constructor(props) {
    super(props);
    this.auth = new Auth();
    this.state = {
      wishList: [],
      dataForDeleteItem: {
        id: null,
        indexToRemove: null,
        showModal: false
      },
      loading: true
    };
  }

  componentDidMount = e => {
    let http = new HttpClass();
    http
      .get("favorite/favorite_list?user_list=" + this.auth.getUserId(), {
        Authorization: this.auth.getToken()
      })
      .then(res => {
        console.log(res);
        this.setState({ wishList: res, loading: false });
      });
  };

  /**
   * Ocultar el modal para eliminar el item de WishList.
   * Esta funcion se lanzara en caso de que se cancele la eliminacion de un item en la wishlist.
   *
   */
  hideTheModalAlert = e => {
    this.setState({
      dataForDeleteItem: {
        showModal: false
      }
    });
  };

  /**
   * Que funcionalidad se usara cuando de click en el boton eliminar en cada ficha de la WishList
   * Lanzar modal y preguntar si esta seguro de eliminar el actual item de el wishlist.
   */
  handleRemoveItemStep1 = (id, indexToRemove) => {
    this.setState({
      dataForDeleteItem: {
        id: id,
        indexToRemove: indexToRemove,
        showModal: true
      }
    });
  };

  /**
   * En caso que el modal para eliminar el item del wishlist, confirme que esta seguro, se lanzara esta funcion
   */
  handleRemoveItemStep2 = () => {
    let headers = {
      Authorization: this.auth.getToken(),
      "Content-Type": "application/json"
    };
    let http = new HttpClass();
    let idToRemove = this.state.dataForDeleteItem["id"];
    http.setBodyAsJSONStringify();
    http
      .post(
        "favorite/remove",
        {
          favorite_id: idToRemove
        },
        headers
      )
      .then(dataRes => {
        if (!dataRes.error && dataRes.code === "fav_deleted") {
          let newArr = [...this.state.wishList];

          /** Que numero de el wishlist va a desaperecer del DOM. */
          let indexToRemove = this.state.dataForDeleteItem["indexToRemove"];

          if (indexToRemove > -1) {
            newArr.splice(indexToRemove, 1);
            this.setState({ wishList: newArr });
          }

          this.setState({
            dataForDeleteItem: {
              id: null,
              indexToRemove: null,
              showModal: false
            }
          });
        }
      });
  };

  render() {
    let imgSrc = process.env.REACT_APP_BACKEND_URL + "/images/products/350x350";

    if (this.state.loading) {
      return (
        <section className="section" id="section_wish_list">
          <Spinner />
        </section>
      );
    }

    if (this.state.wishList.length > 0) {
      return (
        <section id="section_wish_list" className="section">
          <Modal
            title="Eliminar favorito?"
            continue_class="is-danger"
            continue_text="Estoy seguro"
            cancel_text="Cancelar"
            onContinue={this.handleRemoveItemStep2}
            onCancel={this.hideTheModalAlert}
            show={this.state.dataForDeleteItem["showModal"]}
          >
            <p className="is-2">Esta seguro de eliminar este item?</p>
          </Modal>

          {this.state.wishList.map((wl, index) => {
            return (
              <WishListSingle
                key={wl.favorite_id}
                src={imgSrc + wl.product_image}
                title={wl.product_name}
                price={wl.product_price}
                created_at={wl.created_at}
                onRemove={this.handleRemoveItemStep1.bind(
                  null,
                  wl.favorite_id,
                  index
                )}
              />
            );
          })}
        </section>
      );
    } else {
      return (
        <section className="section" id="section_wish_list">
          <Message type="is-info" show={true}>
            Tu lista de favoritos esta actualmente vacia
          </Message>
        </section>
      );
    }
  }
}
export default WishListPage;
