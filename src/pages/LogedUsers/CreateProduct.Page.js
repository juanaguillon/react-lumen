import React from 'react';
import {Redirect} from 'react-router-dom';

import Auth from '../../services/Auth';
import CreateProductForm from '../../components/Forms/CreateProduct.Form';


class CreateProductPage extends React.Component{

  constructor( props ){
    super(props);

    this.state = {
      access: true
    }
  }
  
  componentDidMount(){
    let auth = new Auth();
    if (!auth.isLogged()) {
      this.setState({
        access: false
      });
    }
  }
  
  render( ){
    if ( ! this.state.access ) return <Redirect to="/login"></Redirect>
    
    return (
      <section className="section">
        <h3 className="subtitle is-3 has-text-grey-dark">Crear Producto</h3>
        <CreateProductForm />

      </section>
    );
  }
  
}

export default CreateProductPage;
