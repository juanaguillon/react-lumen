import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

import Layout from "./Layout";
import InitalRoute from "./InititalRoute";
import LoginPage from "./InitalPages/Login.Page";
import RegisterPage from "./InitalPages/Register.Page";
import PrivatePage from "./LogedUsers/Private.Page";

import CreateProductPage from "./LogedUsers/CreateProduct.Page";
import ProductsPage from './PublicPages/Products.Page';
import ProductSinglePage from "./PublicPages/ProductSingle.Page";
import WishListPage from "./LogedUsers/WishList.Page";

/** Crear Una Ruta Privada */

/** Crear Una Ruta Privada */
const PRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        return localStorage.getItem("token_id") !== null ? (
          <Component {...props} />
        ) : (
          <Redirect to="/" />
        );
      }}
    />
  );
};

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/" component={InitalRoute} />
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/register" component={RegisterPage} />
          <PRoute exact path="/private" component={PrivatePage} />

          {/* PRODUCTOS */}
          <Route exact path="/product/all" component={ProductsPage} />
          <Route exact path="/product/id/:id" component={ProductSinglePage} />
          <PRoute
            exact
            path="/product/create"
            component={CreateProductPage}
          />

          {/* FAVORITOS */}
          <Route exact path="/favorite/all" component={WishListPage} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
