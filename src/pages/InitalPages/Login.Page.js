import React from "react";
import { Redirect } from "react-router-dom";

import LoginForm from "../../components/Forms/Login.Form";
import Auth from "../../services/Auth";

const isAuth = () => {
  let auth = new Auth();
  return auth.isLogged();
}

function LoginPage() {
  if (isAuth()) return <Redirect to="/private" />;

  return (
    <section className="section">
      <h3 className="title has-text-dark">Ingresar</h3>
      <div className="box">
        <LoginForm />
      </div>
    </section>
  );
}

export default LoginPage;
