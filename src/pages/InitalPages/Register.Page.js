import React from "react";

import RegisterForm from "../../components/Forms/Register.Form";

class RegisterPage extends React.Component {
  render() {
    return (
      <section className="section">
        <h3 className="title has-text-dark">Registrar</h3>
        <div className="box">
          <RegisterForm />
        </div>
      </section>
    );
  }
}
export default RegisterPage;
