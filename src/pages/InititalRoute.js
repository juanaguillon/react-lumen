/**
 * Este componente se cargará en la ruta inicial, se comprobará si está logeado y mostrara el login o la página privada
 */

import React from 'react';
import Auth from '../services/Auth';

import LoginPage from './InitalPages/Login.Page';
import PrivatePage from './LogedUsers/Private.Page';

/** Esta página se mostrará al momento de inicial la aplicacion en la ruta base ( http://localhost:3000/ ) */

function InitialRoute ( props ){

  let auth = new Auth();
  if ( auth.isLogged() ){
    return <PrivatePage></PrivatePage>
  }else{
    return <LoginPage></LoginPage>
  }
  
} 

export default InitialRoute;