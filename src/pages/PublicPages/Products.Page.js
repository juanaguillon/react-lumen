// React
import React from "react";
import { Link } from "react-router-dom";

// Estilos de actual componente
import "./Products.Page.scss"

// Servicios
import HttpClass from "../../services/Http";
import generalFunctions from "../../services/Functions";

// Otros componentes
import ProductAddToFavButton from "../../components/ProductAddToFavButton";

const ProductCard = props => {

  let gFunctions = generalFunctions
  let href = "/product/id/" + props.id;
  var urlBackend = process.env.REACT_APP_BACKEND_URL + "/images/products/";
  return (
    <Link to={href}>
      <div className="card product_card">
        <div className="card-image">
          <figure className="image is-square">
            <img src={urlBackend + props.thumbnail} alt="Placeholder" />
          </figure>
        </div>
        <div className="card-content">
          <div className="media">
            <div className="media-content">
              <p className="title is-5">{props.title}</p>
            </div>
          </div>

          <div className="content">
            <h5 className="subtitle is-5">{gFunctions.formatCurrency(props.price)}</h5>
            <div className="product_action_card">
              <time dateTime="2016-1-1" className="is-size-7">
                {props.created}
              </time>
              <ProductAddToFavButton productId={props.id} />
            </div>
          </div>
        </div>
      </div>
    </Link>
  );
};

class ProductsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      products: []
    };
  }

  componentDidMount() {
    let http = new HttpClass();
    http.get("product/all").then(products => {
      this.setState({
        products: products
      });
    });
  }

  render() {
    return (
      <div className="section">
        <div className="columns is-multiline">
          {this.state.products.map((product, i) => {
            return (
              <div key={i} className="column is-one-quarter">
                <ProductCard
                  thumbnail={product.product_image350}
                  title={product.product_name}
                  price={product.product_price}
                  created={product.created_at}
                  id={product.id}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default ProductsPage;
