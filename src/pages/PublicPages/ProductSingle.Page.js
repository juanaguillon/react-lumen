import React from "react";
import HttpClass from "../../services/Http";

import "./ProductSingle.Page.scss";
import ProductAddToFavButton from "../../components/ProductAddToFavButton";


const LeftSide = props => {
  let imgSrc = process.env.REACT_APP_BACKEND_URL + "/images/products/";
  return (
    <div className="column is-two-fifths">
      <div className="product_single images_container">
        <figure id="product_image_figure">
          <img src={imgSrc + props.imageSrc} alt="" />
        </figure>
        <div className="product_gallery">
          <div className="product_gallery_wrap">
            <div className="product_gallery_single_container">
              <div className="product_gallery_single_wrap">
                <img src={imgSrc + "350x350" + props.imageSrc} alt="" />
              </div>
            </div>
            {props.gallery.map((image, index) => {
              return (
                <div key={index} className="product_gallery_single_container">
                  <div className="product_gallery_single_wrap">
                    <img src={imgSrc + "350x350" + image} alt="" />
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

const RightSide = props => {
  return (
    <div className="column is-three-fifths">
      <div className="product_single context_container">
        <div className="product_single_title">
          <h2 className="title is-4">{props.title}</h2>
        </div>
        <div className="product_single_description">
          <p>{props.description}</p>
        </div>
        <div className="product_single_price">
          <span className="product_price is-size-5">${props.price}</span>
        </div>
        <div className="product_single_actions">
          <button className="button is-dark">Comprar</button>
          <ProductAddToFavButton productId={props.id} />
        </div>
      </div>
    </div>
  );
};

class ProductSinglePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params["id"],
      product: null,
      loading: true
    };
  }

  componentDidMount() {
    let http = new HttpClass();
    http.get("product/id/" + this.state.id).then(product => {
      this.setState({
        product: product,
        loading: false
      });
    });
  }

  render() {
    if (!this.state.loading) {
      return (
        <section className="section">
          <div className="columns is-tablet">
            <LeftSide
              imageSrc={this.state.product.product_image}
              gallery={this.state.product.product_gallery}
            />
            <RightSide
              title={this.state.product.product_name}
              description={this.state.product.product_description}
              price={this.state.product.product_price}
              id={this.state.product.id}
            />
          </div>
        </section>
      );
    } else {
      return "";
    }
  }
}

export default ProductSinglePage;
