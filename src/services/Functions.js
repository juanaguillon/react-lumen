import * as accounting from "accounting-js";

const generalFunctions = {

  formatCurrency : currency => {
    return accounting.formatMoney(parseInt(currency), {
      symbol: "$",
      precision: 0,
      thousand: "."
    });
  }
  
}

export default generalFunctions;