class Auth {

  getToken() {
    
    return localStorage.getItem("token_id");
  }

  getDecodeToken() {
    var token = this.getToken();
    var base64Url = token.split(".")[1];
    var base64 = decodeURIComponent(
      atob(base64Url)
        .split("")
        .map(function(c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );

    return JSON.parse(base64);
  }

  isLogged() {
    return this.getToken() !== null;
  }

  getUserId(){
    if ( ! this.isLogged ) return;
    return this.getDecodeToken().sub.id;
  }

  closeSession(){
    localStorage.removeItem('token_id');
  }
}

export default Auth;
