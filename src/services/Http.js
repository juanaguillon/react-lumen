class HttpClass {
  /** URL principal donde se enviaran todas las peticiones */
  mainUrl = "";

  /** EndPoint donde se enviara una peticion. */
  url = {};

  /** Headers a enviar a la URL asignada, EJ: "Content-Type:application/json" */
  headers = {};

  /** Metadata POST que se enviará a la URL asignada */
  dataFetch = {};

  /** Se desea enviar los datos como JSON.stringify? */
  asJSONStringify = false;

  /** Query params a enviar en la peticion GET */
  /* queryParams = {}; */

  constructor(mainUrl = "") {
    if (mainUrl === "") {
      this.mainUrl = process.env.REACT_APP_BACKEND_URL;
    } else {
      this.mainUrl = mainUrl;
    }
  }

  /**
   * Enviar una solicitud post a la url establecida.
   * @param {String} url Direccion a enviarse la solicitud
   * @param {any} data Objecto de datos que se enviaran.
   * @param {any} headers? Objeto headers. Si se deja vacio o en false, tomará 'Content-Type' application/json.
   */
  post(url, data, headers = false) {
    if (this.asJSONStringify) {
      data = JSON.stringify(data);
    }

    let dataFetch = {
      method: "post",
      headers: headers,
      body: data
    };
    this.setData(dataFetch);
    this.setUrl(url);
    this.setHeaders(headers);
    return this._createFetch();
  }

  async get(url, headers = false) {
    let dataFetch = {
      method: "get"
    };
    this.setData(dataFetch);
    this.setUrl(url);
    this.setHeaders(headers);
    return await this._createFetch();
  }

  /**
   * Agregar una url para enviar una petición.
   * @param {string} url Url a la cual se enviará la peticion.
   */
  setUrl(url) {
    this.url = url;
  }

  // /**
  //  * Agregar 'Query params' para enviarle a el backend. Este será útil, para enviar variables _GET a el backend y que sea de facil acceso.
  //  * Debe tener en cuenta que este metodo solo se aplicará, si hace una peticion HTTP GET, de lo contrario, no funcionara este metodo y su respectivo uso.
  //  * @param {Object} queryParams Objeto de las nuevas variables GET
  //  */
  // setQueryParams(queryParams) {
  //   this.queryParams = queryParams;
  // }

  /**
   * Datos de Body que se enviarán por peticion FETCH.
   * Tenga en cuenta, que este método no remplazará los headers si se han declarado.
   * @param {any} data Datos clave:valor, para enviar a una url en especifica.
   */
  setData(data) {
    this.dataFetch = data;
  }

  /**
   * Agregar los headers que se enviarán a la petición
   * @param {any} headers Ojeto de headers.
   */
  setHeaders(headers = false) {
    if (!headers) {
      this.headers = {
        "Content-Type": "application/json; charset=utf-8"
      };
    } else {
      this.headers = headers;
    }
  }

  /**
   * Si desea el que body de los datos que se enviarán que codifiquen como JSON.stringify, agregue esta función antes de llamar a la función HTTP
   */
  setBodyAsJSONStringify() {
    this.asJSONStringify = true;
  }

  async _createFetch() {
    this.dataFetch["headers"] = this.headers;
    let fullUrl = this.mainUrl + "/" + this.url;
    return await fetch(fullUrl, this.dataFetch)
      .then(reponse => reponse.json())
      .then(json => json);
  }
}

export default HttpClass;
