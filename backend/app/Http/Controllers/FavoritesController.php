<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FavoriteModel;
use Carbon\Carbon;

class FavoritesController extends Controller
{


  public function create(Request $request)
  {

    $favProduct = $request->input("productID");
    $favUser =  $request->input("userID");
    $modelFavorite = FavoriteModel::where("favorite_product", $favProduct)->where("favorite_user", $favUser)->first();

    if ($modelFavorite) {
      return response()->json(array(
        "error" => true,
        "code" => "product_exists"
      ));
    } else {
      $actualTime = Carbon::now('America/Bogota')->toDateTimeLocalString();

      $dataToSave = array(
        "favorite_product" => $request->input("productID"),
        "favorite_user" =>  $request->input("userID"),
        "created_at" => $actualTime,
        "updated_at" => $actualTime
      );

      if (FavoriteModel::insert($dataToSave)) {
        return response()->json(array(
          "error" => false,
          "code" => "product_added"
        ));
      } else {
        return response()->json(array(
          "error" => true,
          "code" => "server_error"
        ));
      }
    }
  }

  public function favorite_list(Request $request)
  {
    $authList = $request->get("auth");
    if ($request->input("user_list") != $authList["id"]) {
      return response()->json(array(
        "error" => true,
        "code" => "id_not_match",
        "message" => "La lista a obtener no coincide con la sesión actual.",
      ));
    }

    $all = FavoriteModel
      ::join("products", "products.id", "=", "favorites.favorite_product")
      ->where("favorite_user", $authList["id"])
      ->take(10)
      ->get();
    return response()->json($all);
  }

  public function remove_item(Request $request)
  {
    $removeID = $request->input("favorite_id");
    $authList = $request->get("auth");

    $favToRemove = new FavoriteModel();
    $favObject = $favToRemove->find($removeID);


    if ($favObject["favorite_user"] != $authList["id"]) {
      return response()->json(array(
        "error" => true,
        "code" => "id_not_match",
        "message" => "El producto favorito a eliminar no coincide con pertenece a el usuario actual.",
      ));
    } else {
      // Intentar eliminar el actual producto en Favorito
      if ($favObject->delete()) {
        return response()->json(array(
          "error" => false,
          "code" => "fav_deleted",
        ));
      } else {
        return response()->json(array(
          "error" => true,
          "code" => "fav_deleted_error",
        ));
      }
    }
  }
}
