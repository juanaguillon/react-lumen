<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;

class ProductController extends Controller
{

  /**
   * Crear un nuevo producto.
   * PATH: product/create
   */
  public function create(Request $request)
  {
    try {
      
      $this->validate($request, [
        "product_name" => "required|max:255",
        "product_price" => "required|numeric"
      ]);

      // Ruta que se guardará en la base de datos, para ser accesible desde el cliente.
      $product = new Product();
      $dataToSaved = $request->all(array(
        "product_name",
        "product_price",
        "product_description"
      ));

      if (!$request->hasFile("product_image")) {
        $dataToSaved["product_image"] = "default__image.jpg";
      } else {
        
        $principalImage = $request->file( "product_image");
        $fileName = mt_rand(1000, 9999) .  time() . '.' . $principalImage->extension();
        $dataToSaved["product_image"] = $fileName;
        upload_image( $principalImage, $fileName );
      }

      // Guardar la galería de el producto.
      $files_request = array(
        "product_gallery0",
        "product_gallery1",
        "product_gallery2",
        "product_gallery3",
        "product_gallery4"
      );
      $files_gallery = array();

      foreach ($files_request as $file) {
        if ($request->hasFile($file)) {
          $image = $request->file($file);
          $imageFileName = mt_rand(1000, 9999) . time() . '.' . $image->extension();
          $files_gallery[] = $imageFileName ;
          upload_image($image, $imageFileName);
        }
      }

      $dataToSaved["product_gallery"] = json_encode( $files_gallery) ;
      $dataToSaved["created_at"] = Carbon::now("America/Bogota")->toDateTimeString();
      $dataToSaved["updated_at"] = Carbon::now("America/Bogota")->toDateTimeString();
      if ($product::insert($dataToSaved)) {
        return response()->json(
          array(
            "message" => "Se ha creado el producto correctamente",
            "code" => "product_created",
            "error" => false
          )
        );
      } else {
        return response()->json(
          array(
            "message" => "Se ha producido un error al guardar. Intente nuevamente",
            "code" => "product_error",
            "error" => true,
            "error" => ""
          )
        );
      }
    } catch (\Throwable $th) {
      return response()->json(array(
        "error" => $th->getMessage(),
        "file" => $th->getFile(),
        "line" => $th->getLine(),
        "error" => true,
        "message" => "Error en el servidor."
      ));
    }
  }

  /**
   * Obtener todos los productos
   * PATH: product/all
   */
  public function getAllProducts()
  {
    $products = Product::all();

    foreach ($products as $product) {
      $image = $product->product_image;
      if ($image !== "default__image.jpg") {

        $product->product_image200 = "200x200" . $image;
        $product->product_image350 = "350x350" . $image;
      } else {
        $product->product_image200 = $image;
        $product->product_image350 = $image;
      }
    }
    return response()->json($products);
  }

  /**
   * Obtener un producto por ID
   * PATH: product/id/{productId}
   */
  public function getProduct($productId)
  {
    $product = Product::find($productId);

    return response()->json($product);
  }
}
