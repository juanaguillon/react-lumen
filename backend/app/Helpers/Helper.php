<?php

use Intervention\Image\ImageManagerStatic as Image;

/**
 * Crear diferentes miniaturas de una imagen específica.
 * @param Object $image Imagen a ser convertida, debe ser un objeto Intervention\Image\ImageManagerStatic
 * @param String $filename Nombre de archivo a ser guardado
 * @param String $tosave Ruta a ser guardada la imagen, por defecto será carpeta {APP_ROOT}/publuc/images/prodcts
 */
function image_create_different_sizes($image, $filename, $tosave = "")
{

  if ($tosave == "") {
    $tosave = base_path() . '/public/images/products/';
  }

  $image->resize(650, null, function ($constr) {
    $constr->aspectRatio();
  });
  $image->save($tosave . "650x_" . $filename, 76);

  $image->fit(350);
  $image->save($tosave . "350x350" . $filename, 76);
}


function upload_image($image, $filename, $tosave = '')
{

  /**  Guardar las imagenes */
  if ($tosave == "") {
    $tosave = "/products";
  }

  $image->storeAs($tosave, $filename, "uploads");

  $image_to_modify = Image::make($image->getRealPath());
  image_create_different_sizes($image_to_modify, $filename);
}
